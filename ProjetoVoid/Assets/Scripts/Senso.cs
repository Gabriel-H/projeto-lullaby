using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Senso : MonoBehaviour
{

    private GameObject jogador;
    private GameObject andar0;
    private GameObject andar1;
    private GameObject andar2;

    // Start is called before the first frame update
    void Start()
    {
        andar0 = GameObject.FindWithTag("Andar0");
        andar1 = GameObject.FindWithTag("Andar1");
        andar2 = GameObject.FindWithTag("Andar2");
    }

    // Update is called once per frame
    void Update()
    {
        //andar0.SetActive(false);
        //andar2.SetActive(false);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        andar1.SetActive(false);
        andar0.SetActive(true);
    }
}
