using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Experimental.GlobalIllumination;
//using UnityEngine.Windows;

public class Player : MonoBehaviour
{

    private Rigidbody2D rb;

    public float velocidade;

    public float inputHori;
    public float inputVert;

    public bool direcaoLado;

    public bool move;
    public bool run;
    public float stamina;
    public float maxStamina;

    public int vida;
    public int maxVida;

    private GameObject lantena;
    public bool ligar;
    public float bateria;
    public float valor;
    private GameObject pilha;


    private GameObject geladeira;
    public bool abrir;
    public bool recupera;

    private GameObject box;
    public bool esconder;
    public bool dentro;
    private GameObject item;
    public bool pegar;
    public bool jogar;

    private GameObject inimigo;

    public Vector2 anterior;
    public Vector3 posisaoJogar;

    public Animator anim;

    // Start is called before the first frame update
    void Start()
    {
        rb = gameObject.GetComponent<Rigidbody2D>();
        box = GameObject.FindWithTag("Box");
        item = GameObject.FindWithTag("Item");
        inimigo = GameObject.FindWithTag("Inimigo");
        lantena = GameObject.FindWithTag("Lantena");
        pilha = GameObject.FindWithTag("Bateria");
    }

    // Update is called once per frame
    void Update()
    {
        InputGet();
        Flip();
        Lantena();
        animControler();
    }

    void FixedUpdate()
    {
        MoveLogic();
    }

    void InputGet()
    {
        inputHori = Input.GetAxis("Horizontal");
        inputVert = Input.GetAxis("Vertical");

        if(Input.GetKeyDown(KeyCode.E) && abrir == true)
        {
            recupera = true;
            StartCoroutine(Recuperar());
        }
        if (Input.GetKeyDown(KeyCode.E) && recupera == true && abrir == true)
        {
            recupera = false;

        }
    }

    private IEnumerator Recuperar()
    {
        yield return new WaitForSeconds(4f);
        vida++;
    }


    void Flip()
    {
        if(direcaoLado && inputHori > 0) FlipLogic();

        if(!direcaoLado && inputHori < 0) FlipLogic();
    }

    void FlipLogic()
    {
        direcaoLado = !direcaoLado;
        transform.Rotate(0f, -180, 0f);
    }

    void MoveLogic()
    {
        if (move == true)
        {
            rb.velocity = new Vector2(inputHori * velocidade, inputVert * velocidade);
            Run();
            if (Input.GetKey(KeyCode.C)) rb.velocity = rb.velocity / 2;
        }
    }

    void Run()
    {

        if (Input.GetKey(KeyCode.LeftShift) && stamina > 0)
        {
            run = true;
            stamina--;
            rb.velocity = rb.velocity * 2;
        }
        else if (stamina == 0)
        {
            run = false;
            if (!Input.GetKey(KeyCode.LeftShift)) stamina++;
        }
        else if (stamina != maxStamina)
        {
            run = false;
            stamina++;
        }
    }

    void Lantena()
    {
        if (Input.GetKeyDown(KeyCode.F) && bateria > 0)
        {
            ligar = !ligar;
        }
        lantena.SetActive(ligar);
        //lantena.GetComponent<SpotLight>();
        //lantena.innerSpotAngle = ;
        //lantena.pointLightInnerRadius;
        LatenaDirecao();
        Bateria();
    }

    void Bateria()
    {
        if (ligar == true)
        {
            bateria--;
        }
        if (bateria <= 0)
        {
            ligar = false;
        }
        if(Input.GetKeyDown(KeyCode.E) && pegar == true && bateria <= 300)
        {
            bateria = bateria + 100;
            Destroy(pilha.gameObject);
        }
    }

    void LatenaDirecao()
    {
        if (inputVert > 0) lantena.transform.rotation = Quaternion.AngleAxis(0f, Vector3.forward);
        if (inputVert < 0) lantena.transform.rotation = Quaternion.AngleAxis(-180f, Vector3.forward);
        if (inputHori > 0) lantena.transform.rotation = Quaternion.AngleAxis(-90f, Vector3.forward);
        if (inputHori < 0) lantena.transform.rotation = Quaternion.AngleAxis(90f, Vector3.forward);
        if (inputHori > 0 && inputVert > 0) lantena.transform.rotation = Quaternion.AngleAxis(-45f, Vector3.forward);
        if (inputHori > 0 && inputVert < 0) lantena.transform.rotation = Quaternion.AngleAxis(-135f, Vector3.forward);
        if (inputHori < 0 && inputVert > 0) lantena.transform.rotation = Quaternion.AngleAxis(45f, Vector3.forward);
        if (inputHori < 0 && inputVert < 0) lantena.transform.rotation = Quaternion.AngleAxis(135f, Vector3.forward);
    }

    private void OnCollisionEnter2D(Collision2D col)
    {
        if(col.gameObject.layer == LayerMask.NameToLayer("Bateria"))
        {
            pegar = true;
        }

        if(col.gameObject.CompareTag("Inimigo"))
        {
            vida -= col.gameObject.GetComponent<Inimigo>().dano;
            if (vida <= 0)
            {
                Destroy(gameObject);
            }
        }

        if(col.gameObject.layer == LayerMask.NameToLayer("Geladeira"))
        {
            abrir = true;
        }

    }
    private void OnCollisionExit2D(Collision2D col)
    {
        if (col.gameObject.layer == LayerMask.NameToLayer("Bateria"))
        {
            pegar = false;
        }

        if (col.gameObject.layer == LayerMask.NameToLayer("Geladeira"))
        {
            abrir = false;
        }
    }

    void animControler()
    {
        anim.SetFloat("Horizontal", rb.velocity.x);
        anim.SetFloat("Vertical", rb.velocity.y);
    }


}