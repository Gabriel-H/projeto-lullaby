using System.Collections;
using System.Collections.Generic;
using System.Security.Cryptography.X509Certificates;
using UnityEngine;

public class PortaMove : MonoBehaviour
{
    public bool abrir;
    public bool open;
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

        if (Input.GetKeyDown(KeyCode.E) && abrir == true && open == false)
        {
            open = true;
            transform.rotation = Quaternion.AngleAxis(-90f, Vector3.forward);
        }
        else if (Input.GetKeyDown(KeyCode.E) && abrir == true && open == true)
        {
            open = false;
            transform.rotation = Quaternion.AngleAxis(0f, Vector3.forward);
        }
    }

    private void OnCollisionEnter2D(Collision2D col)
    {
        abrir = true;
    }

    private void OnCollisionExit2D(Collision2D col)
    {
        abrir = false;
    }


}
