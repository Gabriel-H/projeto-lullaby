using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine;
using static UnityEngine.EventSystems.EventTrigger;
using UnityEngine.UIElements;

public class Box : MonoBehaviour
{
    private GameObject jogador;
    public bool esconder;
    public bool dentro;

    public float veloplay;
    private Vector2 anterior;
    // Start is called before the first frame update
    void Start()
    {
        jogador = GameObject.FindWithTag("Player");
    }

    // Update is called once per frame
    void Update()
    {

        if (Input.GetKeyDown(KeyCode.E) && esconder == true)
        {
            jogador.SetActive(false);
            esconder = false;
            dentro = true;
            anterior = jogador.transform.position;
            jogador.transform.position = transform.position;
        }
        else if (Input.GetKeyDown(KeyCode.E) && dentro == true)
        {
            jogador.SetActive(true);
            dentro = false;
            jogador.transform.position = anterior;
        }
    }

    private void OnCollisionEnter2D(Collision2D col)
    {
        esconder = true;
    }

    private void OnCollisionExit2D(Collision2D col)
    {
        esconder = false;
    }


}
